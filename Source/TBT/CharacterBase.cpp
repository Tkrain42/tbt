// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterBase.h"
#include "AbilitySystemBase.h"
#include "GameFramework/PlayerController.h"
#include "Classes/AIController.h"
#include "Classes/BrainComponent.h"
#include "Engine/Public/TimerManager.h"
#include "AttributeBase.h"

// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//StatComponent = CreateDefaultSubobject<UStatComponent>(TEXT("Stats"));
	AttributeBase = CreateDefaultSubobject<UAttributeBase>(TEXT("Attributes"));
	AbilitySystem = CreateDefaultSubobject<UAbilitySystemBase>(TEXT("AbilitySystem"));
}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();
	if (AttributeBase)
	{
		AttributeBase->OnHealthChanged.AddDynamic(this, &ACharacterBase::OnHealthChanged);
		AttributeBase->OnManaChanged.AddDynamic(this, &ACharacterBase::OnManaChanged);
		AttributeBase->OnStrengthChanged.AddDynamic(this, &ACharacterBase::OnStrengthChanged);
	}
	
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent * ACharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystem;
}

UAttributeBase * ACharacterBase::GetAttributeBase() const
{
	return AttributeBase;
}

void ACharacterBase::AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire)
{
	if (AbilitySystem)
	{
		if (HasAuthority() && AbilityToAquire)
		{
			AbilitySystem->GiveAbility(FGameplayAbilitySpec(AbilityToAquire));
		}
		AbilitySystem->InitAbilityActorInfo(this, this);
	}
}

void ACharacterBase::AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire)
{
	for (auto AbilityItem : AbilitiesToAquire)
	{
		AquireAbility(AbilityItem);
	}
}

void ACharacterBase::OnHealthChanged(float Health, float MaxHealth)
{
	if ((Health <= 0) && (!bDead))
	{
		bDead = true;
		AddGamePlayTag(TagDead);
		RemoveGamePlayTag(TagFullHealth);
		RemoveGamePlayTag(TagLowHealth);
		
		OnDeath();
	} else if (Health / MaxHealth < .25f)
	{
		AddGamePlayTag(TagLowHealth);
		RemoveGamePlayTag(TagFullHealth);
	} else if (Health == MaxHealth)
	{
		AddGamePlayTag(TagFullHealth);
		RemoveGamePlayTag(TagLowHealth);
	} else
	{
		RemoveGamePlayTag(TagFullHealth);
		RemoveGamePlayTag(TagLowHealth);
	}
	
	K2_OnHealthChanged(Health, MaxHealth);
}

void ACharacterBase::OnManaChanged(float Mana, float MaxMana)
{
	K2_OnManaChanged(Mana, MaxMana);
}

void ACharacterBase::OnStrengthChanged(float Strength, float MaxStrength)
{
	K2_OnStrengthChanged(Strength, MaxStrength);
}

void ACharacterBase::AddGamePlayTag(UPARAM(ref)FGameplayTag & TagToAdd, bool Stack)
{
	AbilitySystem->AddLooseGameplayTag(TagToAdd);
	if (!Stack)
	{
		AbilitySystem->SetTagMapCount(TagToAdd, 1);
	}
}

void ACharacterBase::RemoveGamePlayTag(UPARAM(ref)FGameplayTag & TagToRemove)
{
	AbilitySystem->RemoveLooseGameplayTag(TagToRemove);
}

void ACharacterBase::Stun(float Duration)
{
	bStunned = true;
	DisableInputController();
	AddGamePlayTag(TagStunned);
	GetWorldTimerManager().SetTimer(StunTimerHandle, this, &ACharacterBase::RecoverFromStun, Duration, false);
}

void ACharacterBase::RecoverFromStun()
{
	bStunned = false;
	EnableInputController();
	RemoveGamePlayTag(TagStunned);
}

void ACharacterBase::DisableInputController()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		PC->DisableInput(PC);
	}

	AAIController* AI = Cast<AAIController>(GetController());
	if (AI)
	{
		AI->GetBrainComponent()->StopLogic("Stunned");
	}
}

void ACharacterBase::EnableInputController()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		PC->EnableInput(PC);
	}

	AAIController* AI = Cast<AAIController>(GetController());
	if (AI)
	{
		AI->GetBrainComponent()->RestartLogic();
	}
}

void ACharacterBase::ScaleMaxHealth(float scale)
{
	float NewMax = AttributeBase->GetMaxHealth()*scale;
	AttributeBase->SetMaxHealth(NewMax);
	AttributeBase->SetHealth(NewMax);
}

FIntTile ACharacterBase::TileLocation()
{
	return FIntTile::VectorToTile(GetActorLocation());
}

FIntTile ACharacterBase::VectorToTile(FVector Location)
{
	return FIntTile::VectorToTile(Location);
}

float ACharacterBase::UpdateTurnCounter(float amount)
{
	TurnCounter += amount * AttributeBase->GetSpeed();
	return TurnCounter;
}

float ACharacterBase::ClearTurnCounter()
{
	TurnCounter = 0;
	return TurnCounter;
}

void ACharacterBase::CharacterTurnStart()
{
	ClearTurnCounter();
	BP_CharacterTurnStart();
}

void ACharacterBase::CharacterTurnEnd()
{
	BP_CharacterTurnEnd();
}



