// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CoolTimer.h"
#include "CooldownManagerTypes.h"
#include "CooldownManager.generated.h"

/*
Manager for a custom cooldown timer system.  Each timer is stored in a TMap with a handle as a base, allowing for an endless supply of cooldown timers.  
Best practice:  Create some constants to represent each timer handle desired, so that timers don't get confused with one another.
Stores timers in a FCoolTimer.  Tick updates each timer in the TMap.
*/


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TBT_API UCooldownManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCooldownManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	TMap<int32, FCoolTimer> Cooldowns;

	float SafeLength(float Length);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Cooldowns", meta = (ToolTip = "Creates or resets a cooldown in slot Handle.  "))
	void SetCooldown(UCooldownManagerTypes* CoolTimerID, float Length=1.0f);

	UFUNCTION(BlueprintPure, Category = "Cooldowns", meta = (ToolTip = "Returns the time remaining in the given Handle slot.  Note that if there is no timer in the handle, will return 1."))
	float CooldownAsPercentage(UCooldownManagerTypes* CoolTimerID);

	UFUNCTION(BlueprintPure, Category = "Cooldowns", meta = (ToolTip = "Returns the time remaining in the give Handle.  Note that if there is no timer in the handle, will return Zero."))
	float CooldownTimeRemaining(UCooldownManagerTypes* CoolTimerID);

	UFUNCTION(BlueprintPure, Category = "Cooldowns", meta = (ToolTip = "Returns the elapsed time since the handle was started.  Note that if the handle has not been assigned with SetCooldown, this will return zero."))
	float CooldownTimeElapsed(UCooldownManagerTypes* CoolTimerID);
	
	UFUNCTION(BlueprintPure, Category = "Cooldowns")
	bool InCooldown(UCooldownManagerTypes* CoolTimerID);
};
