// (C) 2018 Brian K. Trotter, mnemoth42@gmail.com

#include "GamePlayAbilityBase.h"

FGameplayAbilityInfo UGamePlayAbilityBase::GetAbilityInfo()
{
	UGameplayEffect* CoolDownEffect = GetCooldownGameplayEffect();
	UGameplayEffect* CostEffect = GetCostGameplayEffect();
	float Cost = 0;
	float CoolDownDuration = 0;
	EAbilityCostType CostType = EAbilityCostType::Health;
	if (CoolDownEffect && CostEffect)
	{
		
		CoolDownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, CoolDownDuration);
		
		
		if (CostEffect->Modifiers.Num() > 0)
		{
			FGameplayModifierInfo EffectInfo = CostEffect->Modifiers[0];
			EffectInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Cost);
			FGameplayAttribute CostAttr = EffectInfo.Attribute;
			FString AttributeName = CostAttr.AttributeName;
			if (AttributeName == "Health")
			{
				CostType = EAbilityCostType::Health;
			} else if (AttributeName == "Mana")
			{
				CostType = EAbilityCostType::Mana;
			} else if (AttributeName == "Strength")
			{
				CostType = EAbilityCostType::Strength;
			}
		}
	}
	return FGameplayAbilityInfo(CoolDownDuration, Cost, CostType, UIMaterial, GetClass(), CancelTag);
}