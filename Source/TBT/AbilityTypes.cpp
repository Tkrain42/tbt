// (C) 2018 Brian K. Trotter, mnemoth42@gmail.com

#include "AbilityTypes.h"

FGameplayAbilityInfo::FGameplayAbilityInfo()
	:CooldownDuration(0),
	Cost(0),
	CostType(EAbilityCostType::Mana),
	UIMat(nullptr),
	AbilityClass(nullptr)
{

}

FGameplayAbilityInfo::FGameplayAbilityInfo(float InCooldownDuration, 
	float InCost, 
	EAbilityCostType InCostType, 
	UMaterialInstance * InUIMat, 
	TSubclassOf<UGameplayAbilityBase> InAbilityClass, 
	FGameplayTag InCancelTag)
	:CooldownDuration(InCooldownDuration),
	Cost(InCost),
	CostType(InCostType),
	UIMat(InUIMat),
	AbilityClass(InAbilityClass),
	CancelTag(InCancelTag)
{
}
