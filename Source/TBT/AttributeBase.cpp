// Fill out your copyright notice in the Description page of Project Settings.

#include "AttributeBase.h"
#include "GameplayEffectTypes.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

UAttributeBase::UAttributeBase():
	Health(250),
	MaxHealth(250),
	Mana(150),
	MaxMana(150),
	Strength(100),
	MaxStrength(100),
	MeleeAttackBase(20),
	MeleeDefenseBase(10),
	MeleeAttackMultiplier(1),
	MeleeDefenseMultiplier(1),
	MagicAttackBase(20),
	MagicDefenseBase(10),
	MagicAttackMultiplier(1),
	MagicDefenseMultiplier(1),
	RangedAttackBase(20),
	RangedDefenseBase(10),
	RangedAttackMultiplier(1),
	RangedDefenseMultiplier(1),
	Speed(1) 
{

}

void UAttributeBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData & Data)
{
	Super::PostGameplayEffectExecute(Data);
	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{

		Health.SetCurrentValue(FMath::Clamp(Health.GetCurrentValue(), 0.0f, MaxHealth.GetCurrentValue()));
		Health.SetBaseValue(FMath::Clamp(Health.GetBaseValue(), 0.0f, MaxHealth.GetBaseValue()));
		OnHealthChanged.Broadcast(Health.GetCurrentValue(), MaxHealth.GetCurrentValue());
	}

	if (Data.EvaluatedData.Attribute == GetManaAttribute())
	{

		Mana.SetCurrentValue(FMath::Clamp(Mana.GetCurrentValue(), 0.0f, MaxMana.GetCurrentValue()));
		Mana.SetBaseValue(FMath::Clamp(Mana.GetBaseValue(), 0.0f, MaxMana.GetBaseValue()));
		OnManaChanged.Broadcast(Mana.GetCurrentValue(), MaxMana.GetCurrentValue());
	}

	if (Data.EvaluatedData.Attribute == GetStrengthAttribute())
	{

		Strength.SetCurrentValue(FMath::Clamp(Strength.GetCurrentValue(), 0.0f, MaxStrength.GetCurrentValue()));
		Strength.SetBaseValue(FMath::Clamp(Strength.GetBaseValue(), 0.0f, MaxStrength.GetBaseValue()));
		OnStrengthChanged.Broadcast(Strength.GetCurrentValue(), MaxStrength.GetCurrentValue());
	}
}

float UAttributeBase::GetHealthAsPercentage()
{
	return GetHealth() / GetMaxHealth();
}

float UAttributeBase::GetManaAsPercentage()
{
	return GetMana() / GetMaxMana();
}

float UAttributeBase::GetStrengthAsPercentage()
{
	return GetStrength() / GetMaxStrength();
}
