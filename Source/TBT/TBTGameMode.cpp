// (C) 2018 Brian K. Trotter

#include "TBTGameMode.h"
#include "Engine/World.h"
#include "Tilebase.h"
#include "CharacterBase.h"
#include "Kismet/GameplayStatics.h"

ATBTGameMode::ATBTGameMode()
{
	Tiles.Reset();
}

FIntTile ATBTGameMode::CalculateTileLocation(AActor* ActorToLocate)
{
	if (ActorToLocate)
	{
		return FIntTile(ActorToLocate->GetActorLocation());
	}
	return FIntTile(0,0);
}

FVector ATBTGameMode::CalculateIdealTileWorldPosition(FIntTile TileLocation)
{
	return TileLocation.TileToLocation();
}

int32 ATBTGameMode::ScanLevelForTiles()
{
	Tiles.Reset();
	TArray<AActor*> FoundTiles;
	FoundTiles.Reset();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATileBase::StaticClass(), FoundTiles);
	for (auto Actor : FoundTiles)
	{
		ATileBase* Tile = Cast<ATileBase>(Actor);
		if (Tile)
		{
			if (!FoundTiles.Contains(Tile->TileLocation)) //Prevent Duplicates
			{
				Tiles.Add(Tile->TileLocation(), Tile);
				UE_LOG(LogTemp, Warning, TEXT("Adding %i, %i"), Tile->TileLocation().X, Tile->TileLocation().Y)
			}
		}
	}
	return Tiles.Num();
}

ATileBase * ATBTGameMode::GetTileAtLocation(FVector Location)
{
	auto Key = FIntTile::VectorToTile(Location);
	if (Tiles.Contains(Key))
	{
		return Tiles[Key];
	}
	return nullptr;
}

ATileBase * ATBTGameMode::GetTileAtCoordinate(FIntTile Coordinate)
{
	if (Tiles.Contains(Coordinate))
	{
		return Tiles[Coordinate];
	}
	return nullptr;
}

TArray<FIntTile> ATBTGameMode::GetPathToLocation(FIntTile Start, FIntTile Finish)
{
	UE_LOG(LogTemp, Warning, TEXT("Testing path from %i,%i to %i,%i"), Start.X, Start.Y, Finish.X, Finish.Y)
		if (Start == Finish)
		{
			UE_LOG(LogTemp, Warning, TEXT("Hey, those two locations are identical, so... I'm ignoring you big time."))
			return TArray<FIntTile>();
		}
	if (Tiles.Contains(Start) && Tiles.Contains(Finish))
	{
		TMap<FIntTile, FIntTile> Links;
		Links.Reset();
		TArray<FIntTile> Queue;
		Queue.Add(Start);
		bool Found = false;
		while (!Found && Queue.Num() > 0)
		{
			auto TileToSearch = Queue[0];
			UE_LOG(LogTemp, Warning, TEXT("Testing %i,%i"), TileToSearch.X, TileToSearch.Y )
			FIntTile North = TileToSearch.North();
			FIntTile South = TileToSearch.South();
			FIntTile East = TileToSearch.East();
			FIntTile West = TileToSearch.West();
			if (TryAddLink(Links, North, TileToSearch))
			{
				TryAddQueue(Queue, North);
				if (North.SameTile(Finish))
				{
					Found = true;
				}
			}
			if (TryAddLink(Links, South, TileToSearch))
			{
				TryAddQueue(Queue, South);
				if (South.SameTile(Finish))
				{
					Found = true;
				}
			}
			if (TryAddLink(Links, West, TileToSearch))
			{
				TryAddQueue(Queue, West);
				if (West.SameTile(Finish))
				{
					Found = true;
				}
			}
			if (TryAddLink(Links, East, TileToSearch))
			{
				TryAddQueue(Queue, East);
				if (East.SameTile(Finish))
				{
					Found = true;
				}
			}
			
			Queue.RemoveAt(0);
		}
		if (Found)
		{
			TArray<FIntTile> tempresult;
			tempresult.Reset();
			tempresult.Add(Finish);
			auto current = Finish;
			while (!current.SameTile(Start))
			{
				current = Links[current];
				//UE_LOG(LogTemp, Warning, TEXT("Reverse Trace (%i,%i)"), current.X, current.Y)
				tempresult.Insert(current,0);
			}
			
			return tempresult;
		}
	}
	return TArray<FIntTile>();
}

void ATBTGameMode::TryAddQueue(TArray<FIntTile> &Queue, FIntTile &North)
{
	if (!Queue.Contains(North))
	{
		Queue.Add(North);
		//UE_LOG(LogTemp, Warning, TEXT("Queuing %i, %i"), North.X, North.Y)
	}
}

bool ATBTGameMode::TryAddLink(TMap<FIntTile, FIntTile> &Links, FIntTile &North, FIntTile &TileToSearch)
{
	if (Tiles.Contains(North))
	{ //if(!Tiles[North]->HasContents())
		{
			if (!Links.Contains(North))
			{
				//UE_LOG(LogTemp, Warning, TEXT("Adding %i,%i to Links."), North.X, North.Y)
				Links.Add(North, TileToSearch);
				return true;
			}
			return false;
		}
	}
	return false;
}

TArray<ACharacterBase*> ATBTGameMode::GatherEntities()
{
	TArray<AActor*> FoundActors;
	FoundActors.Reset();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACharacterBase::StaticClass(), FoundActors);
	Entities.Reset();
	for (AActor* actor : FoundActors)
	{
		ACharacterBase* Character = Cast<ACharacterBase>(actor);
		if (Character)
		{
			Entities.Add(Character);
		}
	}
	
	return Entities;
}

ACharacterBase* ATBTGameMode::GetNextEntity()
{
	if (CurrentEntity)
	{
		CurrentEntity->CharacterTurnEnd();
	}
	GatherEntities();
	if (Entities.Num() <= 0)
	{
		return nullptr;
	}
	int32 NextEntity = -1;
	float Threshhold = 1.0f;
	while (NextEntity < 0)
	{
		for (int32 i = 0; i < Entities.Num(); i++)
		{
			
			Entities[i]->UpdateTurnCounter(.1f);
			UE_LOG(LogTemp, Warning, TEXT("Turn Counter %s, %f"), *Entities[i]->GetName(), Entities[i]->TurnCounter)
			if (Entities[i]->TurnCounter > Threshhold)
			{
				UE_LOG(LogTemp, Warning, TEXT("Advancing %s"), *Entities[i]->GetName())
				Threshhold = Entities[i]->TurnCounter;
				NextEntity = i;
			}
		}
	}
	CurrentEntity = Entities[NextEntity];
	CurrentEntity->ClearTurnCounter();
	CurrentEntity->CharacterTurnStart();
	for (auto Tile : Tiles)
	{
		Tile.Value->OnCurrentEntitySwitched();
	}
	return CurrentEntity;
}

bool ATBTGameMode::CheckLocationForEntity(FIntTile Location)
{
	bool result = false;
	for (ACharacterBase* Entity : Entities)
	{
		if (Entity->TileLocation().SameTile(Location))
		{
			result = true;
		}
	}
	return result;
}


