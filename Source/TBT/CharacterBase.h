// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "TBTileInterface.h"
#include "CharacterBase.generated.h"

class UCooldownManager;
class UStatComponent;
class UAttributeBase;

UCLASS()
class TBT_API ACharacterBase : public ACharacter, public IAbilitySystemInterface, public ITBTileInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
	UAttributeBase* AttributeBase=nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities")
	UAbilitySystemComponent* AbilitySystem=nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
		float AttackRange = 30;

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const;
	
	UFUNCTION(BlueprintPure, Category="Attributes")
	UAttributeBase* GetAttributeBase() const;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UAnimMontage*> MeleeAttackMontages;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UAnimMontage*> RangedAttackMontages;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UAnimMontage*> HealingMontages;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UAnimMontage*> DashMontages;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UAnimMontage*> GroundShakeMontages;

	UFUNCTION(BlueprintCallable, Category = "CharacterBase")
	void AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire);

	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void AquireAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Combat")
	void SetWeaponStatus(bool Attacking);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Team = 1;

	UPROPERTY(BlueprintReadOnly)
	bool bDead = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Health")
	FGameplayTag TagDead;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Health")
	FGameplayTag TagFullHealth;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Health")
	FGameplayTag TagLowHealth;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Status")
	FGameplayTag TagStunned;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Skill")
	FGameplayTag TagHeal;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Skill")
	FGameplayTag TagAttack;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Tags|Skill")
	FGameplayTag TagDash;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Abilities")
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire;

	UFUNCTION()
	void OnHealthChanged(float Health, float MaxHealth);
	UFUNCTION()
	void OnManaChanged(float Mana, float MaxMana);
	UFUNCTION()
	void OnStrengthChanged(float Strength, float MaxStrength);

	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnHealthChanged"))
	void K2_OnHealthChanged(float Health, float MaxHealth);
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnManaChanged"))
	void K2_OnManaChanged(float Mana, float MaxMana);
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnStrengthChanged"))
	void K2_OnStrengthChanged(float Strength, float MaxStrength);
	UFUNCTION(BlueprintImplementableEvent)
	void OnDeath();
	
	UFUNCTION(BlueprintCallable, Category = "Tags")
	void AddGamePlayTag(UPARAM(ref) FGameplayTag& TagToAdd, bool Stack=false);
	
	UFUNCTION(BlueprintCallable, Category = "Tags")
	void RemoveGamePlayTag(UPARAM(ref) FGameplayTag& TagToRemove);

	UPROPERTY(BlueprintReadOnly, Category = "Stun")
	bool bStunned = false;

	FTimerHandle StunTimerHandle;

	UFUNCTION(BlueprintCallable, Category = "Stun")
	void Stun(float Duration);

	void RecoverFromStun();

	UFUNCTION(BlueprintCallable, Category = "Control")
		void DisableInputController();

	UFUNCTION(BlueprintCallable, Category = "Control")
		void EnableInputController();

	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable, Category = "Combat")
		void Impulse_Attack();

	UFUNCTION(BlueprintCallable, Category = "Buff")
		void ScaleMaxHealth(float scale);

	UFUNCTION(BlueprintPure, Category = "Tile")
		FIntTile TileLocation();

	UFUNCTION(BlueprintPure, Category = "Tile")
		FIntTile VectorToTile(FVector Location);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Turn")
		float TurnCounter = 0.0f;

	UFUNCTION(BlueprintCallable, Category = "Turn")
		float UpdateTurnCounter(float amount);

	UFUNCTION(BlueprintCallable, Category = "Turn")
		float ClearTurnCounter();

	UFUNCTION(BlueprintCallable, Category = "Turn")
		void CharacterTurnStart();
	UFUNCTION(BlueprintImplementableEvent,  Category = "Turn", meta = (ToolTip = "This should not be called directly from blueprint.  Call ACharacterBase->CharacterTurnStart() and it will call this event."))
		void BP_CharacterTurnStart();
	UFUNCTION(BlueprintCallable, Category = "Turn")
		void CharacterTurnEnd();
	UFUNCTION(BlueprintImplementableEvent,   Category = "Turn", meta = (ToolTip = "Note:  This should not be called directly.  Instead call ACharacterBase::CharacterTurnEnd()"))
		void BP_CharacterTurnEnd();

	
};
