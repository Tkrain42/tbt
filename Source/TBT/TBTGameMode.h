// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TileStructures.h"
#include "TBTGameMode.generated.h"

class ATileBase;
class ACharacterBase;




/**
 * 
 */
UCLASS()
class TBT_API ATBTGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	float DefaultTileSize = 200;

	TMap<FIntTile, ATileBase*> Tiles;

public:
	ATBTGameMode();

	UFUNCTION(BlueprintCallable, Category = "Tile")
	FIntTile CalculateTileLocation(AActor* ActorToLocate);

	
	

	UFUNCTION(BlueprintCallable, Category = "Tile")
	FVector CalculateIdealTileWorldPosition(FIntTile TileLocation);

	UFUNCTION(BlueprintCallable, Category = "Tile|Initialization")
	int32 ScanLevelForTiles();
	
	UFUNCTION(BlueprintPure, Category = "Tile", meta = (ToolTip = "Returns a TileBase located at the specified Vector coordinates.  Returns nullptr if none exist."))
	ATileBase* GetTileAtLocation(FVector Location);

	UFUNCTION(BlueprintPure, Category = "Tile", meta = (ToolTip = "Returns a TileBase at the specified IntTile (X,Y) coordinates.  Returns nullptr if none exists.  Note that this is the coordinates on the tile grid, not the standard coordinates built into Unreal."))
	ATileBase* GetTileAtCoordinate(FIntTile Coordinate);

	UFUNCTION(BlueprintCallable, Category = "Tile")
	TArray<FIntTile> GetPathToLocation(FIntTile Start, FIntTile Finish);

	void TryAddQueue(TArray<FIntTile> &Queue, FIntTile &North);

	bool TryAddLink(TMap<FIntTile, FIntTile> &Links, FIntTile &North, FIntTile &TileToSearch);

	UFUNCTION(BlueprintCallable, Category = "Turns")
		TArray<ACharacterBase*> GatherEntities();

	UPROPERTY(BlueprintReadWrite, Category = "Turns")
	TArray<ACharacterBase*> Entities;

	UPROPERTY(BlueprintReadWrite, Category = "Turns")
	ACharacterBase* CurrentEntity=nullptr;

	UFUNCTION(BlueprintCallable, Category = "Turns")
	ACharacterBase* GetNextEntity();

	UFUNCTION(BlueprintPure, Category = "Turns")
	bool CheckLocationForEntity(FIntTile Location);

};
