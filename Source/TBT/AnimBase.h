// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "AnimBase.generated.h"

/**
 * 
 */
UCLASS()
class TBT_API UAnimBase : public UAnimInstance
{
	GENERATED_BODY()
protected:

	UFUNCTION(BlueprintPure)
	ACharacterBase* GetCharacterBase();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FVector AdjustedVelocity=FVector(0.0f,0.0f,0.0f);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bInMotion = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float fSpeed = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float fDirection = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bFalling=false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bDead = false;

	UFUNCTION(BlueprintCallable)
	void SetVariables(class ACharacterBase* CB);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bStunned = false;
	
};
