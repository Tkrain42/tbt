// (C) 2018 Brian K. Trotter, mnemoth42@gmail.com

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "AbilitySystemInterface.h"
#include "AbilityTypes.h"
#include "GamePlayAbilityBase.generated.h"

class ACharacterBase;
class UCharacterMovementComponent;

/**
 * 
 */
UCLASS(Blueprintable)
class TBT_API UGamePlayAbilityBase : public UGameplayAbility
{
	GENERATED_BODY()
	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	UMaterialInstance* UIMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	bool AddToUI = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FGameplayTag CancelTag;

	UFUNCTION(BlueprintCallable, Category = "AbilityBase")
		FGameplayAbilityInfo GetAbilityInfo();



};
