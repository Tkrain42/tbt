// Fill out your copyright notice in the Description page of Project Settings.

#include "AnimBase.h"
#include "CharacterBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"

ACharacterBase* UAnimBase::GetCharacterBase()
{
	auto pawn = TryGetPawnOwner();
	if (pawn)
	{
		return Cast<ACharacterBase>(pawn);
	}
	return nullptr;
}

void UAnimBase::SetVariables(ACharacterBase * CB)
{
	if (!CB)
	{
		return;
	}
	bFalling = CB->GetCharacterMovement()->IsFalling();
	auto Velocity= CB->GetVelocity();
	fSpeed = Velocity.Size();
	bInMotion = (fSpeed != 0.0f);
	auto rotation = CB->GetActorRotation();
	fDirection = CalculateDirection(Velocity, rotation);
	AdjustedVelocity.X = fDirection;
	AdjustedVelocity.Y = fSpeed;
	bDead = CB->bDead;
	bStunned = CB->bStunned;
	
}
