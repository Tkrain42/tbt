// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "TileStructures.h"
#include "TBTileInterface.generated.h"

UINTERFACE(Blueprintable)
class UTBTileInterface : public UInterface
{
	GENERATED_BODY()
};

class ITBTileInterface
{
	GENERATED_BODY()
public:
	
	FIntTile TileLocation();
	

	
};

