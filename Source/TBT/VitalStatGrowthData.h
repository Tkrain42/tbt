// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "VitalStatGrowthData.generated.h"

/**
 * 
 */
UCLASS()
class TBT_API UVitalStatGrowthData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 1, ToolTip = "This is the level 1 stat.  It is also used as the root of the calculation", UIMin = 1))
	float Base = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.1, ToolTip = "Used to scale growth of stat.  Result = Base*Level*Scalar.  Lower to slow growth, raise for rapid growth", UIMin = 0.1))
	float LevelScalar = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.01, ToolTip = "Used with StatBasedonLevelAndBuff", UIMin = 0.01))
	float BuffScalar = 0.01f;

	UFUNCTION(BlueprintPure)
	float StatBasedOnLevel(int Level);

	UFUNCTION(BlueprintPure)
	float StatBasedOnLevelAndBonus(int Level, int Bonus);
};
