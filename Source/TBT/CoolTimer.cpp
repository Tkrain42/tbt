// (C) 2018 Brian K. Trotter

#include "CoolTimer.h"

void FCoolTimer::StartCooldown(float cooldownlength)
{
	length = FMath::Max(cooldownlength, .001f);
	current = 0.0f;
}

void FCoolTimer::UpdateCooldown(float deltaTime)
{
	current += deltaTime;
}

float FCoolTimer::TimeRemaining() const
{
	return length - FMath::Min(current, length);
}

float FCoolTimer::TimeElapsed() const
{
	return current;
}

float FCoolTimer::Length() const
{
	return length;
}

float FCoolTimer::TimeRemainingAsPercentage() const
{
	if (length <= 0.0f)
	{
		return 0;
	}
	return FMath::Min(current / length, 1.0f);
}

bool FCoolTimer::InCooldown() const
{
	return current < length;
}
