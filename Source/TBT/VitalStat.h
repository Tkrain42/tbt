// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VitalStat.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FVitalStat
{
	GENERATED_BODY()

		FVitalStat();
	
	UPROPERTY(BlueprintReadWrite)
	float Current=1;
	UPROPERTY(BlueprintReadWrite)
	float Max=1;

	float GetStatAsPercentage()
	{
		return FMath::Min(Current / Max, 1.0f);
	}

	float GetCurrent()
	{
		return Current;
	}

	float SetCurrent(float Value)
	{
		Current = FMath::Clamp(Value, 0.0f, Max);
		return Current;
	}

	float Add(float ValueToAdd)
	{
		return SetCurrent(Current + ValueToAdd);
	}

	float SetMax(float Value)
	{
		Max = FMath::Min(Value, 1.0f);
		Current = FMath::Min(Current, Max);
		return Max;
	}

	bool Maxed()
	{
		return Current== Max;
	}

	bool Depleted()
	{
		return Current == 0.0f;
	}

};
