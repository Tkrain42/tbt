// (C) 2018 Brian K. Trotter

#include "TileBase.h"

// Sets default values
ATileBase::ATileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorLocation(TileIdealLocation());
	Location = TileLocation();
}

// Called when the game starts or when spawned
void ATileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FIntTile ATileBase::TileLocation()
{
	return FIntTile(GetActorLocation());
}

FVector ATileBase::TileIdealLocation()
{
	return FIntTile::TileToVector(TileLocation());
}

void ATileBase::OnCurrentEntitySwitched()
{
	K2_OnCurrentEntitySwitched();
}

bool ATileBase::HasContents()
{
	TestContents();
	return bOccupied;
}

