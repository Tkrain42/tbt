// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "AttributeBase.generated.h"

// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	UFUNCTION(BlueprintPure)\
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	UFUNCTION(BlueprintCallable)\
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	UFUNCTION(BlueprintCallable)\
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStatChangedDelegate, float, StatCurrent, float, StatMax);

/**
 * 
 */
UCLASS()
class TBT_API UAttributeBase : public UAttributeSet
{
	GENERATED_BODY()
	
public:

	UAttributeBase();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UAttributeBase, Health)
	

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MaxHealth)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Melee")
	FGameplayAttributeData MeleeAttackBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MeleeAttackBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Melee")
	FGameplayAttributeData MeleeDefenseBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MeleeDefenseBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Melee")
	FGameplayAttributeData MeleeAttackMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MeleeAttackMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Melee")
	FGameplayAttributeData MeleeDefenseMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MeleeDefenseMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Magic")
	FGameplayAttributeData MagicAttackBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MagicAttackBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Magic")
	FGameplayAttributeData MagicDefenseBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MagicDefenseBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Magic")
	FGameplayAttributeData MagicAttackMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MagicAttackMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Magic")
	FGameplayAttributeData MagicDefenseMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MagicDefenseMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Ranged")
	FGameplayAttributeData RangedAttackBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, RangedAttackBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Ranged")
	FGameplayAttributeData RangedDefenseBase;
	ATTRIBUTE_ACCESSORS(UAttributeBase, RangedDefenseBase)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Ranged")
	FGameplayAttributeData RangedAttackMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, RangedAttackMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Ranged")
	FGameplayAttributeData RangedDefenseMultiplier;
	ATTRIBUTE_ACCESSORS(UAttributeBase, RangedDefenseMultiplier)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Mana")
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UAttributeBase, Mana)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Mana")
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MaxMana)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Strength")
	FGameplayAttributeData Strength;
	ATTRIBUTE_ACCESSORS(UAttributeBase, Strength)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Strength")
	FGameplayAttributeData MaxStrength;
	ATTRIBUTE_ACCESSORS(UAttributeBase, MaxStrength)

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Attribute|Speed")
	FGameplayAttributeData Speed;
	ATTRIBUTE_ACCESSORS(UAttributeBase, Speed)

	void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;

	UFUNCTION(BlueprintPure, Category = "Attribute|Health")
	float GetHealthAsPercentage();

	UFUNCTION(BlueprintPure, Category = "Attribute|Mana")
	float GetManaAsPercentage();

	UFUNCTION(BlueprintPure, Category = "Attribute|Strength")
		float GetStrengthAsPercentage();

	FOnStatChangedDelegate OnHealthChanged;
	FOnStatChangedDelegate OnManaChanged;
	FOnStatChangedDelegate OnStrengthChanged;
};
