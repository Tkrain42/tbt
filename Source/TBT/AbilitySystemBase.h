// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemBase.generated.h"

/**
 * 
 */
UCLASS()
class TBT_API UAbilitySystemBase : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
};
