// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "CoolTimer.generated.h"

/**
 *  Base structure for a cooldown timer system.  
 */
USTRUCT(BlueprintType)
struct FCoolTimer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "This is the running count from the moment it was started"))
	float current=1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.001, ToolTip = "This is the length of the cooldown.  NEVER set to 0", UIMin = 0.001))
	float length=1.0f;

	
	void StartCooldown(float cooldownlength=1.0f);

	
	void UpdateCooldown(float deltaTime);

	
	float TimeRemaining() const;

	
	float TimeElapsed() const;

	
	float Length() const;

	
	float TimeRemainingAsPercentage() const;

	
	bool InCooldown() const;

};
