// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LevelStat.generated.h"
/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStatLevelChanged);

USTRUCT(BlueprintType)
struct FLevelStat
{
	GENERATED_BODY()

		FLevelStat();

	UPROPERTY(BlueprintReadWrite, Category="LevelStat")
	int32 Experience;

	UPROPERTY(BlueprintReadWrite, Category="LevelStat")
	int32 Level;

	
	UPROPERTY(BlueprintReadWrite, Category = "Calculation", meta = (ClampMin = 0.1, ToolTip = "Used in ExpToNextLevel calculations", UIMin = 0.1))
		float scalar=1.0f;

	UPROPERTY(BlueprintReadWrite, Category = "Calculation", meta = (ToolTip = "Used in calculating ExperienceToNextLevel"))
	int32 basecost=100;

	
	int32 ExperienceToNextLevel() const;

	bool AddExperience(int32 ExperienceToAdd);

	float ExperienceAsPercentage() const;

	FOnStatLevelChanged OnStatLevelChanged;
};
