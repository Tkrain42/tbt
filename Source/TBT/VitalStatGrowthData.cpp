// Fill out your copyright notice in the Description page of Project Settings.

#include "VitalStatGrowthData.h"

float UVitalStatGrowthData::StatBasedOnLevel(int Level)
{
	return Base * Level*LevelScalar;
}

float UVitalStatGrowthData::StatBasedOnLevelAndBonus(int Level, int Bonus)
{
	return StatBasedOnLevel(Level) + (Base*Level*BuffScalar);
}
