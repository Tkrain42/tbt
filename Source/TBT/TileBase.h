// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TBTileInterface.h"
#include "TileStructures.h"
#include "TileBase.generated.h"

UCLASS()
class TBT_API ATileBase : public AActor, public ITBTileInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATileBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Tile")
	FIntTile TileLocation();

	UFUNCTION(BlueprintPure, Category = "Tile")
	FVector TileIdealLocation();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	FIntTile Location;

	UFUNCTION(BlueprintCallable)
	void OnCurrentEntitySwitched();

	UFUNCTION(BlueprintImplementableEvent, Category = "Tile", meta = (DisplayName = "OnCurrentEntitySwitched"))
	void K2_OnCurrentEntitySwitched();

	UFUNCTION(BlueprintImplementableEvent, Category = "Contents")
		void TestContents();
	UFUNCTION(BlueprintCallable, Category = "Contents")
		bool HasContents();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bOccupied = false;

	
};
