// (C) 2018 Brian K. Trotter

#include "TileStructures.h"

FIntTile::FIntTile():
	X(0),
	Y(0)
{
}

FIntTile::FIntTile(int32 inX, int32 inY):
	X(inX), Y(inY)
{
}

FIntTile::FIntTile(FVector Location)
{
	
	X = FMath::RoundToFloat((Location.X)/200.0f);
	Y = FMath::RoundToFloat((Location.Y)/200.0f);
}

FVector FIntTile::TileToLocation(float inZ)
{
	return FVector(X*200.0f, Y*200.0f, inZ);
}

FIntTile FIntTile::LocationToTile(FVector Location)
{
	int32 XX = FMath::TruncToInt((Location.X)/ 200.0f);
	int32 YY = FMath::TruncToInt((Location.Y)/ 200.0f);
	
	return FIntTile(XX,YY);
}

FVector FIntTile::TileToVector(FIntTile tile, float inZ)
{
	return FVector(tile.X*200.0f, tile.Y*200.0f, inZ);
}

FIntTile FIntTile::VectorToTile(FVector Location)
{
	return FIntTile(Location);
}

int32 FIntTile::CalcIndex()
{
	return X + Y * 4096;
}

bool FIntTile::SameTile(FIntTile Other)
{
	return (X == Other.X && Y == Other.Y);
}

FIntTile FIntTile::North()
{
	return FIntTile(X - 1, Y);
}

FIntTile FIntTile::South()
{
	return FIntTile(X+1,Y);
}

FIntTile FIntTile::East()
{
	return FIntTile(X,Y-1);
}

FIntTile FIntTile::West()
{
	return FIntTile(X,Y+1);
}
