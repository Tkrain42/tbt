// Fill out your copyright notice in the Description page of Project Settings.

#include "CooldownManagerTypes.h"

UCooldownManagerTypes::UCooldownManagerTypes()
{
	Handle = FMath::RandRange(1000, 350000);
}

int32 UCooldownManagerTypes::H()
{
	return Handle;
}
