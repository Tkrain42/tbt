// (C) 2018 Brian K. Trotter

#pragma once

#include "CoreMinimal.h"
#include "TileStructures.generated.h"

USTRUCT(BlueprintType)
struct FIntTile
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 X;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 Y;

	FIntTile();
	FIntTile(int32 inX, int32 inY);
	FIntTile(FVector Location);

	FVector TileToLocation(float inZ=0.0f);
	FIntTile LocationToTile(FVector Location);
	
	
	static FVector TileToVector(FIntTile tile, float inZ = 0.0f);
	
	static FIntTile VectorToTile(FVector Location);
	
	static FIntTile IntToFIntTile(int32 TileToLocate);
	int32 CalcIndex();
	bool SameTile(FIntTile Other);

	bool operator==(const FIntTile& Other) const
	{
		return Other.X == X && Other.Y == Y;
	}


	FIntTile North();
	FIntTile South();
	FIntTile East();
	FIntTile West();

};

FORCEINLINE uint32 GetTypeHash(const FIntTile& b)
{
	return FCrc::MemCrc32(&b, sizeof(FIntTile));
}
