// Fill out your copyright notice in the Description page of Project Settings.

#include "CooldownManager.h"

// Sets default values for this component's properties
UCooldownManager::UCooldownManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCooldownManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

//Prevention mechanism, any time a Timer length is set, it must first be passed through SafeLength.  This guarantees that it will NEVER be zero, which would cause a catastrophic crash.
//There are prevention methods already in the GetCooldownAsPercentage, but this is an added layer of insurance.

float UCooldownManager::SafeLength(float Length)
{
	return FMath::Max(Length, .001f);
}


// Called every frame
void UCooldownManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Structs don't have tick methods, so we're making do with an UpdateCooldown method called on each timer in the system.
	for (auto cooldown: Cooldowns)
	{
		cooldown.Value.UpdateCooldown(DeltaTime);
	}
	// ...
}

//The actual functionality of the cooldown is handled in the CoolTimer class.  Here, we're just resetting a CoolTimer if it exists already, or if it doesn't exist we're creating one.
void UCooldownManager::SetCooldown(UCooldownManagerTypes* CoolTimerID, float Length)
{
	if (Cooldowns.Contains(CoolTimerID->Handle))
	{
		Cooldowns[CoolTimerID->Handle].StartCooldown(SafeLength(Length));
	} else
	{
		FCoolTimer NewCooldown;
		NewCooldown.StartCooldown(Length);
		Cooldowns.Add(CoolTimerID->Handle, NewCooldown);
	}
}

//This function just serves as a wrapper, the CoolTime calculates the actual percentage.
float UCooldownManager::CooldownAsPercentage(UCooldownManagerTypes* CoolTimerID)
{
	if (Cooldowns.Contains(CoolTimerID->Handle))
	{
		return Cooldowns[CoolTimerID->Handle].TimeRemainingAsPercentage();
	}
	return 1.0f; //If there is no handle existing, that means that that handle is not current in cooldown.
}

//As above, this function just acts as a wrapper for the CoolTime class.  
float UCooldownManager::CooldownTimeRemaining(UCooldownManagerTypes* CoolTimerID)
{
	if (Cooldowns.Contains(CoolTimerID->Handle))
	{
		return Cooldowns[CoolTimerID->Handle].TimeRemaining();
	}
	return 0.0f;
}

float UCooldownManager::CooldownTimeElapsed(UCooldownManagerTypes* CoolTimerID)
{
	if (Cooldowns.Contains(CoolTimerID->Handle))
	{
		return Cooldowns[CoolTimerID->Handle].TimeElapsed();
	}
	return 0.0f;
}

bool UCooldownManager::InCooldown(UCooldownManagerTypes * CoolTimerID)
{
	if (Cooldowns.Contains(CoolTimerID->Handle))
	{
		return (Cooldowns[CoolTimerID->Handle].InCooldown());
	}
	return false;
}

