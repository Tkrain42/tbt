// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelStat.h"

FLevelStat::FLevelStat()
{

}

int32 FLevelStat::ExperienceToNextLevel() const
{
	int result = basecost;
	for (int i = 1; i < Level ; i++)
	{
		result += basecost * result*scalar;
	}
	return result;
}

bool FLevelStat::AddExperience(int32 ExperienceToAdd)
{
	Experience += ExperienceToAdd;
	bool levelUp = false;
	while (Experience > ExperienceToNextLevel())
	{
		Experience -= ExperienceToNextLevel();
		Level++;
		levelUp = true;
		OnStatLevelChanged.Broadcast();
	}
	return levelUp;
}

float FLevelStat::ExperienceAsPercentage() const
{
	return (float)Experience / (float)ExperienceToNextLevel();
}
