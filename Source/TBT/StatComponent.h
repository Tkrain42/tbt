// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalStat.h"
#include "LevelStat.h"
#include "VitalStatGrowthData.h"
#include "StatComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, CurrentHealth, float, MaxHealth);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnManaChanged, float, CurrentMana, float, MaxMana);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChanged, float, CurrentStamina, float, MaxStamina);



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TBT_API UStatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Self explanatory... for alive or dead.
	
	FVitalStat Health;
	//For casting magic type spells.
	
	FVitalStat Mana;
	//For performing feats of strength
	
	FVitalStat Stamina;
	
	FLevelStat Level;

	UVitalStatGrowthData* VSDHealth = nullptr;
	UVitalStatGrowthData* VSDMana = nullptr;
	UVitalStatGrowthData* VSCEndurance = nullptr;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Health")
	float AddHealth(float HealthToAdd);

	UFUNCTION(BlueprintPure, Category="Health")
	float GetHealth();

	UFUNCTION(BlueprintPure, Category="Health")
	float GetHealthAsPercentage();

	UFUNCTION(BlueprintCallable, Category="Health")
	float SetHealthMax(float NewMax);

	UFUNCTION(BlueprintCallable, Category="Health")
	float SetHealth(float NewHealth);

	UFUNCTION(Blueprintpure, Category="Health")
	float GetHealthMax() const;
	
	FOnHealthChanged OnHealthChanged;
	FOnManaChanged OnManaChanged;
	FOnStaminaChanged OnStaminaChanged;
		
};
