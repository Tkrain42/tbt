// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CooldownManagerTypes.generated.h"

/**
 * 
 */
UCLASS()
class TBT_API UCooldownManagerTypes : public UPrimaryDataAsset
{
	GENERATED_BODY()

		UCooldownManagerTypes();

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Handle = 0;
	UFUNCTION(BlueprintPure)
		int32 H();
	
};
